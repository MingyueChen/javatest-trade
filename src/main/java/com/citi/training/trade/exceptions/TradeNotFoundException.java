package com.citi.training.trade.exceptions;

@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {
	 public TradeNotFoundException(String msg) {
	        super(msg);
	    }
}
