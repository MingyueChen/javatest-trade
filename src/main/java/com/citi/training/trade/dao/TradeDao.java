package com.citi.training.trade.dao;

import java.util.List;

import com.citi.training.trade.model.Trade;


public interface TradeDao {
	List<Trade> findAll();
    int create(Trade Trade);
    Trade findById(int id);
    void deleteById(int id);
}
