package com.citi.training.trade.dao.mysql;

//import static net.logstash.logback.argument.StructuredArguments.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import com.citi.training.trade.dao.TradeDao;
import com.citi.training.trade.exceptions.TradeNotFoundException;
import com.citi.training.trade.model.Trade;


/**
 * <h1>This is the MysqlTradeDao Class</h1>
 * This class implements create, retrieve, and delete functions
 * 
 * This class is related to {@link com.citi.training.trade.dao.mysql}
 * 
 * @author Mingyue Chen
 *
 */

@Component
public class MysqlTradeDao implements TradeDao {
	 private static final Logger logger = LoggerFactory.getLogger(MysqlTradeDao.class);
	 @Autowired
	    JdbcTemplate tpl;

	    public List<Trade> findAll(){
	    	 logger.info("find all trades ");
	        return tpl.query("select id, stock, price,volume from trade",
	                         new TradeMapper());
	    }

	    @Override
	    public Trade findById(int id) {
	        List<Trade> Trades = this.tpl.query(
	                "select id, stock, price,volume from trade where id = ?",
	                new Object[]{id},
	                new TradeMapper()
	        );
	        if(Trades.size() <= 0) {
	        	 throw new TradeNotFoundException ("Cannot find the trade with id: " + id);
	        }
	        logger.info("find trade with id " + Trades.get(0).getId());
	        return Trades.get(0);
	    }

	    @Override
	    public int create(Trade Trade) {
	        KeyHolder keyHolder = new GeneratedKeyHolder();
	        this.tpl.update(
	            new PreparedStatementCreator() {
	                @Override
	                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	                    PreparedStatement ps =
	                            connection.prepareStatement("insert into trade (stock, price,volume) values (?, ?, ?)",
	                            Statement.RETURN_GENERATED_KEYS);
	                    ps.setString(1, Trade.getStock());
	                    ps.setDouble(2, Trade.getPrice());
	                    ps.setInt(3, Trade.getVolume());
	                    return ps;
	                }
	            },
	            keyHolder);
	        logger.info("insert a new trade " + Trade.getStock());
	        return keyHolder.getKey().intValue();
	    }

	    @Override
	    public void deleteById(int id) {
	    	List<Trade> trades = this.tpl.query(
	                 "select * from trade where id = ?",
	                 new Object[]{id},
	                 new TradeMapper()
	         );
	         if(trades.size() <= 0) {
	        	// throw an exception
	        	 throw new TradeNotFoundException ("Trade with id " + id + " is not found. Cannot delete it.");
	         }
	        logger.info("delete a trade with id " +  trades.get(0).getId());
	        this.tpl.update("delete from trade where id=?", id);
	    }

	    private static final class TradeMapper implements RowMapper<Trade> {
	        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
	            return new Trade(rs.getInt("id"),
	                                rs.getString("stock"),
	                                rs.getDouble("price"),
	            					rs.getInt("volume"));
	        }
	    }
}
