package com.citi.training.trade.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trade.dao.TradeDao;
import com.citi.training.trade.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTests {

	@Autowired
	private TradeService productService;
	
	@MockBean
	private TradeDao mockTradeDao;
	
	@Test
	public void test_createRuns() {
		int newId = 1;
		Trade testTrade = new Trade(23, "Walmart", 67.2,5295);
		
		when(mockTradeDao.create(any(Trade.class))).thenReturn(newId);
		//Trade otherTrade = new Trade(2, "BreakTest", 1000);
		int createdId = productService.create(testTrade);
		
		verify(mockTradeDao).create(testTrade);
		assertEquals(newId, createdId);
	}
	
	@Test
	public void test_deleteRuns() {
		productService.deleteById(5);
		verify(mockTradeDao).deleteById(5);
	}
	
	@Test
	public void test_findByIdRuns() {
		int testId = 66;
		
		Trade testTrade = new Trade(23, "Walmart", 67.2,5295);

		when(mockTradeDao.findById(testId)).thenReturn(testTrade);
		Trade returnTrade = productService.findById(testId);
		assertEquals(testTrade, returnTrade);
	}
	
	@Test
	public void test_findAll() {
		List<Trade> testList = new ArrayList<Trade>();
		testList.add(new Trade(3, "Costco", 81,8790));
		
		when(mockTradeDao.findAll()).thenReturn(testList);
		List<Trade> returnedList = productService.findAll();
		
		verify(mockTradeDao).findAll();
		assertEquals(testList, returnedList);
	}

}
