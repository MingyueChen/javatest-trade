package com.citi.training.trade.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;


import com.citi.training.trade.exceptions.TradeNotFoundException;
import com.citi.training.trade.model.Trade;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTests {

	  @SpyBean
	    JdbcTemplate tpl;

	    @Autowired
	    MysqlTradeDao mysqlTradeDao;

	    @Test
	    public void test_createAndFindAll_works() {
	        mysqlTradeDao.create(new Trade(1, "Google", 112.5, 5710));
	        assertThat(mysqlTradeDao.findAll().size(), equalTo(1));
	    }

	    @Test
	    public void test_createAndFindById_works() {
	        int newId = mysqlTradeDao.create(
	        		new Trade(1, "Google", 112.5, 5710));
	        assertNotNull(mysqlTradeDao.findById(newId));
	    }

	    @Test(expected=TradeNotFoundException.class)
	    public void test_createAndFindById_throwsNotFound() {
	        mysqlTradeDao.create(new Trade(1, "Google", 112.5, 5710));
	        mysqlTradeDao.findById(4);
	    }

}
