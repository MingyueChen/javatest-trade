package com.citi.training.trade.model;
import org.junit.Test;


public class TradeTests {
	@Test
    public void test_Trade_fullConstructor() {
        Trade testTrade = new Trade(0, "Google", 100, 2000);
        assert(testTrade.getId() == 0);
        assert(testTrade.getStock().equals("Google"));
        assert(testTrade.getPrice() == 100);
        assert(testTrade.getVolume() == 2000);
    }

    @Test
    public void test_Trade_setters() {
    	Trade testTrade = new Trade(0, "Google", 100, 2000);
        testTrade.setId(10);
        testTrade.setStock("Amazon");
        testTrade.setPrice(101);
        testTrade.setVolume(500);

        assert(testTrade.getId() == 10);
        assert(testTrade.getStock().equals("Amazon"));
        assert(testTrade.getPrice() == 101);
        assert(testTrade.getVolume() == 500);
    }

    @Test
    public void test_Trade_toString() {
    	Trade testTrade = new Trade(0, "Google", 100, 2000);
        assert(testTrade.toString() != null);
        assert(testTrade.toString().contains(testTrade.getStock()));
    }
}
